# PostgreSQL cluster testing

Trying to stand up a 2 node Postgres cluster with pgadmin already connected and built in replication working

## Run

```bash
docker-compose up -d
```

The browse to http://localhost:5050

Username/Password is admin@local and admin

Databases should already be connected.  You will have to type in the password for the servers, which is pgword. 

When done, run the following which also cleans up the volumes

```bash
docker-compose down -v
```

## Dumping servers.json

```bash
docker exec -it `docker ps | grep pgadmin | cut -c 1-5` sh
/pgadmin4 # python /pgadmin4/setup.py --user admin@local --dump-servers output.json
Configuration for 2 servers dumped to output.json.
/pgadmin4 # cat output.json 
...
```

Passwords can not be imported or exported https://www.pgadmin.org/docs/pgadmin4/4.13/import_export_servers.html#json-format


## Attribution

I found the following sites/projects helpful

- https://github.com/ManabuMiwa/postgres-pgadmin-docker
- https://wiki.postgresql.org/wiki/Replication,_Clustering,_and_Connection_Pooling
- https://stackoverflow.com/questions/33309121/using-docker-compose-to-create-tables-in-postgresql-database
- https://github.com/bitnami/bitnami-docker-postgresql/blob/master/docker-compose-replication.yml
- https://github.com/paunin/PostDock/tree/master/docker-compose